(ns semperos.gitdemo.impl.fs-test
  (:require [clojure.test :refer :all]
            [semperos.gitdemo.impl.fs :refer :all]
            [clojure.spec.test :as stest])
  (:import clojure.lang.ExceptionInfo))

(stest/instrument 'semperos.gitdemo.fs/git-repository?)

(deftest test-git-repository
  (is (git-repository "."))
  (is (not (git-repository (System/getProperty "java.io.tmpdir")))))
