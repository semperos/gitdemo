(ns semperos.gitdemo.git-test
  (:require [clojure.test :refer :all]
            [semperos.gitdemo.git :refer :all]
            [clojure.spec.test :as stest]
            [clojure.string :as str]
            [semperos.gitdemo.protocols :as api])
  (:import semperos.gitdemo.git.GitRepository))

(stest/instrument 'semperos.gitdemo.git/rev-list)
(stest/instrument 'semperos.gitdemo.git/format-log-entry)

(def test-raw-log-entry
  "commit 438f918796cf07c2f5dcd518832f8497f0162b93\nAuthor: Daniel Gregoire <daniel.l.gregoire@gmail.com>\nDate:   2016-06-25 02:26:07 -0400\n\n    Add pulsar GOL pattern\n\ndiff --git a/gol.scm b/gol.scm\nindex ad9a5d0..2b50648 100644\n--- a/gol.scm\n+++ b/gol.scm\n@@ -115,6 +115,25 @@\n                     (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))\n             reps)))\n \n+(define run-pulsar\n+  (lambda (reps)\n+    (conway (quote ((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 1 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 1 1 1 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 1 0 1 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 1 1 1 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 1 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n+                    (0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)))\n+            reps)))\n+\n (define run-repeat\n   (lambda (reps)\n     (conway (quote ((0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)\n")

(def test-rev-list
  ["5b998d2fbdfe0e09be55e8c2d77082902a3e5c68"
   "438f918796cf07c2f5dcd518832f8497f0162b93"
   "c0e2bc8858008b6b8627cf79d5b9d8bfc6fcc04e"
   "23827fb5789e9258234bc82b023e004f78a871cf"])

(def test-file-contents
  "abcd\nefgh\nijkl")

(deftype MockRepo [path-name]
  api/IGit
  (file [this commit file-name]
    test-file-contents)
  (rev-list [this]
    test-rev-list))

(def test-repo (MockRepo. (repository ".git")))

(deftest test-revlist
  (is (= test-rev-list
         (api/rev-list test-repo))))

(deftest test-format-log-entry
  ;; Exercise specs
  (format-log-entry test-repo (str/split-lines test-raw-log-entry)))

(deftest test-file
  (is (= test-file-contents
         (api/file test-repo "438f918796cf07c2f5dcd518832f8497f0162b93" "fake.txt"))))

(deftest ^:integration test-real-rev-list
  (is (= test-rev-list
         (take 4 (api/rev-list (GitRepository. (repository "/tmp/gitdemo-test")))))))

(deftest ^:integration test-real-file
  (is (= "*.pyc\n"
         (api/file (GitRepository. (repository "/tmp/gitdemo-test"))
                   "438f918796cf07c2f5dcd518832f8497f0162b93" ".gitignore")))

  (is (= 201
         (count (str/split-lines (api/file (GitRepository. (repository "/tmp/gitdemo-test"))
                                           "5b998d2" "LICENSE.txt"))))))
