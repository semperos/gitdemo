(defproject com.semperos/gitdemo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :globals {*warn-on-reflection* true}
  :dependencies [[org.clojure/clojure "1.9.0-alpha8"]
                 [enlive "1.1.6"]
                 [me.raynes/fs "1.4.6"]
                 [hiccup "1.0.5"]]
  :main ^:skip-aot semperos.gitdemo
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :test-selectors {:default (fn [m] true)
                   :integration :integration
                   :ci (complement (some-fn :integration))})
