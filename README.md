# GitDemo: Slides from Git Commits

Takes `git diff` output for a repo and turns it into slides.

Slides are stored as EDN. The output layer is pluggable, with built-in support for HTML (with JS niceties).

## Usage

```
lein do clean, run <git-repo-directory> <output-directory> && open <output-directory>/slideshow.html
```

## Features

 * Take Git repo directory as input
 * Run Git commands to get, per commit with filter:
    * Diff
    * Content of files

## Implementation Steps

### Preprocessing

 * [x] Entry-point taking Git directory location, validate exists
 * [ ] Accept optional whitelist or blacklist filter for file names
 * [ ] EDN representation of commit log with pointers to files containing per-commit:
    * [ ] Diff content
    * [ ] Whole-file content
 * [ ] Protocol-based output layer for publishing
    * [ ] HTML output with JS niceties

### HTML Output

 * Webserver-less
 * SPA or per-page SPA
 * Title at top
 * Text on left/right
 * Code on opposite half, with syntax highlighting
 * Dynamic toggle to allow code to:
    * Go from half to full-slide in size
    * Toggle diff styling on/off
    * Toggle whether diff context or entire file is shown

## License

Copyright © 2016 Daniel Gregoire (semperos)

The code in this repository is licensed under the Apache License (Version 2.0).
