(ns semperos.gitdemo
  (:require [semperos.gitdemo.git :refer [->GitRepository repository]]
            [semperos.gitdemo.protocols :as api]
            [semperos.gitdemo.publish :as publish]
            [semperos.gitdemo.impl.fs :as fs])
  (:gen-class))

(defn slideshow
  [repo-dir]
  (let [repo (->GitRepository (repository repo-dir))
        revs (reverse (api/rev-list repo))
        commits (partition 2 1 revs)
        initial-slide (api/log-entry repo (first revs))]
    (reduce
     (fn [acc [commit-a commit-b]]
       (update acc
               :slides
               conj
               (api/log-entry repo commit-a commit-b)))
     {:slides [initial-slide]}
     commits)))

(defn html
  [edn]
  (publish/html-slideshow edn))

(defn -main
  "Generate a slideshow from a source `repo-dir` and persist it to `target-dir`"
  [& [repo-dir target-dir :as args]]
  (println "Starting Slideshow generation using repo at" repo-dir)
  (try
    (let [slideshow (slideshow repo-dir)]
      (spit (str target-dir (fs/separator) "slideshow.edn")
            (pr-str slideshow))
      (println "Slideshow data collection complete. Check" (str target-dir (fs/separator) "slideshow.edn") "for your raw data.")
      (println "Publishing to HTML format...")
      (let [{:keys [output copy-resources]} (html slideshow)]
        (spit (str target-dir (fs/separator) "slideshow.html")
              output)
        (fs/copy-resources copy-resources target-dir)))
    (System/exit 0)
    (catch Throwable t
      (binding [*out* *err*]
        (println "Something went horribly wrong:" t)
        (println "FAILED to generate slideshow.")
        (System/exit 1)))
    (finally (shutdown-agents))))
