(ns semperos.gitdemo.impl.html
  "HTML utilities used by publish layer"
  (:require [net.cgrand.enlive-html :as temp :refer [content deftemplate html-snippet]]
            [hiccup.core :refer :all]))

(defn slide-html
  [body]
  (html
    [:section
     [:h1 (str "Slide " (java.util.UUID/randomUUID))]
     [:p body]]))

(defn slides
  [{:keys [slides] :as slideshow}]
  (map
    (fn [slide]
      (slide-html (:commit/message slide)))
    slides))

(deftemplate slideshow-template "reveal.js-template/gitdemo.html"
  [slides]
  [:div.reveal :div.slides] (content (apply html-snippet slides)))

(defn render
  [nodes]
  (apply str nodes))

(defn slideshow
  [slideshow]
  {:output (render (slideshow-template (slides slideshow)))
   :copy-resources #{"reveal.js-template/css"
                     "reveal.js-template/js"
                     "reveal.js-template/lib"}})
