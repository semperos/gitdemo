(ns semperos.gitdemo.impl.fs
  "File-system related functions."
  (:require [clojure.string :as str]
            [clojure.spec :as s]
            [clojure.java.io :as io]
            [me.raynes.fs :refer [copy copy-dir]])
  (:import [java.nio.file Files FileSystem FileSystems
            LinkOption Path Paths]))

(def link-options (into-array LinkOption []))
(def string-options (into-array String []))

(defn separator
  []
  (.getSeparator (FileSystems/getDefault)))

(defn- exists?
  [path]
  (Files/exists path link-options))

(defn- directory?
  [path]
  (Files/isDirectory path link-options))

(defn- path?
  [path]
  (instance? Path path))

(s/def ::path? path?)

(defn- present-directory?
  [path-name]
  (let [path (Paths/get path-name string-options)]
    (and (exists? path)
         (directory? path)
         path)))

(defn git-repository
  [path-name]
  (let [path-name (if (str/ends-with? path-name "/")
                    (subs path-name 0 (dec (count path-name)))
                    path-name)]
    (present-directory? (str path-name (separator) ".git"))))

(s/fdef git-repository
        :args (s/cat :path-name string?)
        :ret (s/or :true ::path? :false #{false}))

(defn copy-resources
  "Copy, taking directories into account."
  [resources target-dir]
  (doseq [resource resources
          :let [path (Paths/get (.toURI (io/resource resource)))]]
    (if (Files/isDirectory path link-options)
      (copy-dir (.toFile path) target-dir)
      (copy (.toFile path) target-dir))))
