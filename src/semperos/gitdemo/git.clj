(ns semperos.gitdemo.git
  "Git-related functions"
  (:require [clojure.string :as str]
            [clojure.java.shell :refer [sh]]
            [clojure.spec :as s]
            [semperos.gitdemo.protocols :as api]
            [semperos.gitdemo.impl.fs :as fs])
  (:import [java.nio.file Path]
           [java.time LocalDateTime ZoneId]
           [java.time.format DateTimeFormatter]
           [java.util Date]))

(deftype GitRepository [^Path path])

(defn ->GitRepository
  [path]
  (if-not path
    (throw (ex-info (str "No Git repository found at " path)
                    {:path path}))
    (GitRepository. path)))

(def git-repository? (partial extends? api/IGit))

(def sha-pattern #"[a-f0-9]{40}")

(defn sha?
  [s]
  (and (string? s)
       (= (count s) 40)
       (re-find sha-pattern s)))

(def author-pattern #"Author:\s+([^<]+)(<[^>]+>)")
(def date-time-pattern #"Date:\s+(.*)")
(def date-time-formatter (DateTimeFormatter/ofPattern "yyyy-MM-dd HH:mm:ss x"))
(def lines-affected-pattern #"@@\s+-((\d+),(\d+))\s+\+((\d+),(\d+))\s+@@")

(defn first-match
  [lines starting-text]
  (first (filter #(str/starts-with? % starting-text) lines)))

(defn parse-commit-sha
  [lines]
  (let [commit-line (first-match lines "commit")
        commit-sha (re-find sha-pattern commit-line)]
    commit-sha))

(defn parse-author
  [lines]
  (let [author-line (first-match lines "Author:")
        [_ name email] (re-find author-pattern author-line)
        name (str/trim name)
        ;; Remove angle brackets
        email (subs email 1 (dec (count email)))]
    {:commit/author-name name
     :commit/author-email email}))

(defn parse-date-time
  [lines]
  (let [date-time-line (first-match lines "Date:")
        [_ date-time] (re-find date-time-pattern date-time-line)]
    (java.util.Date/from (.toInstant
                          (.atZone (LocalDateTime/parse date-time date-time-formatter)
                                   (ZoneId/systemDefault))))))

(defn parse-message
  [lines]
  (->> lines
       (take-while (fn [line] (not (str/starts-with? line "diff --git"))))
       next ;; skip commit sha
       (drop-while (fn [line] (or (str/starts-with? line "Author:")
                                 (str/starts-with? line "Date:")
                                 (str/blank? line))))
       drop-last ;; remove final line break
       (map str/trim)
       (str/join "\n")))

(defn parse-diff-statement
  [lines]
  (first-match lines "diff --git"))

(defn parse-index
  [lines]
  (first-match lines "index "))

;; TODO This breaks on `@@ -0,0 +1 @@`
(defn parse-file-legend
  "Lines \"--- a/foo.txt\" \"+++ b/foo.txt\""
  [lines]
  (let [first-file (first-match lines "---")
        second-file (first-match lines "+++")
        first-id (if (str/ends-with? first-file "/dev/null")
                   "/dev/null"
                   (subs first-file 4 5))
        first-name (if (= first-id "/dev/null")
                     "/dev/null"
                     (subs first-file 6))
        second-id (subs second-file 4 5)
        second-name (subs second-file 6)
        ;; file-lines-affected-line (first-match lines "@@")
        ;; [_ _ original-line-start original-num-lines
        ;;  _   modified-line-start modified-num-lines] (re-find lines-affected-pattern file-lines-affected-line)
        ;; original-line-start (Long/parseLong original-line-start)
        ;; original-line-end (+ original-line-start (Long/parseLong original-num-lines))
        ;; modified-line-start (Long/parseLong modified-line-start)
        ;; modified-line-end (+ modified-line-start (Long/parseLong modified-num-lines))
        ;; original-lines-affected [original-line-start original-line-end]
        ;; modified-lines-affected [modified-line-start modified-line-end]
        ]
    {:diff/original {:diff/file-id first-id
                     :diff/file-name first-name
                     ;; :diff/lines-affected original-lines-affected
                     }
     :diff/modified {:diff/file-id second-id
                     :diff/file-name second-name
                     ;; :diff/lines-affected modified-lines-affected
                     }}))

(defn parse-content-line
  [line]
  (let [starts-with? (partial str/starts-with? line)]
    (cond
      (starts-with? "+") {:line/content (str " " (subs line 1)) :line/type :diff/modified}
      (starts-with? "-") {:line/content (str " " (subs line 1)) :line/type :diff/original}
      :else {:line/content line :line/type :diff/context})))


(defn parse-content
  [lines]
  (let [pre-content-line (first-match lines "@@")
        idx (.indexOf ^clojure.lang.APersistentVector lines pre-content-line)
        content-lines (subvec lines (inc idx))]
    ;; TODO I think this is the right place to do this. Reconsider as needed.
    (mapv parse-content-line content-lines)))

(defn parse-diffs
  [lines]
  (let [diffs (->> lines
                   (partition-by #(str/starts-with? % "diff --git"))
                   next
                   (partition 2)
                   (mapv second)
                   (map (partial into [])))]
    (mapv
     (fn [diff-lines]
       {;; :diff/statement (parse-diff-statement lines)
        :diff/index (parse-index diff-lines)
        :diff/file-legend (parse-file-legend diff-lines)
        :diff/content (parse-content diff-lines)
        :diff/raw-content (str/join "\n" diff-lines)})
     diffs)))

(defn full-files
  "Get full file content for files included in diffs of commit, at commit."
  [^GitRepository repo commit-sha diffs]
  (reduce
   (fn [acc {:keys [diff/file-legend] :as diff}]
     (let [file-name (get-in file-legend [:diff/modified :diff/file-name])
           file-content (api/file repo commit-sha file-name)]
       (assoc acc file-name file-content)))
   {}
   diffs))

(defn format-log-entry
  [^GitRepository repo lines]
  (let [commit-sha (parse-commit-sha lines)
        diffs (parse-diffs lines)
        files (full-files repo commit-sha diffs)]
    {:commit/sha commit-sha
     :commit/author (parse-author lines)
     :commit/date-time (parse-date-time lines)
     :commit/message (parse-message lines)
     :commit/diffs diffs
     :commit/files files}))

(defn log-entry
  ([^GitRepository repo commit]
   (log-entry repo commit nil))
  ([^GitRepository repo start-commit end-commit]
   (let [target (if end-commit
                  (str start-commit ".." end-commit)
                  start-commit)
         {:keys [out exit]} (sh "git" "log" "-p" "--date=iso" target
                                :dir (.toFile (api/path repo)))]
     (when-not (zero? exit)
       (throw (ex-info "Failed to call `git log`."
                       {:repo repo
                        :start-commit start-commit
                        :end-commit end-commit})))
     (format-log-entry repo (str/split-lines out)))))

(s/def :commit/sha sha?)
(s/def :commit/author-name string?)
(s/def :commit/author-email string?)
(s/def :commit/author (s/keys :req [:commit/author-name :commit/author-email]))
(s/def :commit/date-time inst?)
(s/def :commit/message string?)
;; (s/def :commit/diff-statement string?)
(s/def :diff/index string?) ;; TODO Understand this portion of output
(s/def :diff/file-id keyword?)
(s/def :diff/file-name string?)
(s/def :diff/lines-affected (s/coll-of number? :kind vector? :count 2))
(s/def :diff/file (s/keys :req [:diff/file-id :diff/file-name :diff/lines-affected]))
(s/def :diff/original :diff/file)
(s/def :diff/modified :diff/file)
(s/def :diff/file-legend (s/keys :req [:diff/original :diff/modified]))
(s/def :diff/content (s/coll-of string? :kind vector?))
(s/def :diff/raw-content string?)
(s/def :commit/diff (s/keys :req [:diff/index :diff/file-legend :diff/content :diff/raw-content]))
(s/def :commit/diffs (s/coll-of :commit/diff))

(s/def ::log-entry
  (s/keys :req [:commit/sha :commit/author :commit/date-time :commit/message
                :commit/diffs]))

(s/fdef format-log-entry
        :args (s/cat :repo git-repository? :lines (s/coll-of string? :kind vector?))
        :ret (s/coll-of ::log-entry :kind vector? :min-count 1))

(defn rev-list
  [^GitRepository repo]
  (let [{:keys [out exit]} (sh "git" "rev-list" "HEAD"
                               :dir (.toFile (api/path repo)))]
    (when-not (zero? exit)
      (throw (ex-info "Failed to call `git rev-list HEAD`."
                      {:repo repo})))
    (str/split-lines out)))

(s/fdef rev-list
        :args (s/cat :git-repository git-repository?)
        :ret (s/coll-of ::commit-sha :kind vector? :min-count 1))

(defn repository
  [path]
  (if (string? path)
    (fs/git-repository path)
    path))

(defn file
  [^GitRepository repo commit-sha file-name]
  (let [{:keys [out exit]} (sh "git" "show" (str commit-sha ":" file-name)
                               :dir (.toFile (api/path repo)))]
    (when-not (zero? exit)
      (throw (ex-info "Failed to call `git show <commit>:<file-name>`."
                      {:repo repo
                       :commit-sha commit-sha
                       :file-name file-name})))
    out))

(extend-type GitRepository
  api/IGit
  (file [this commit-sha file-name]
    (file this commit-sha file-name))
  (log-entry
    ([this commit]
     (log-entry this commit))
    ([this start-commit end-commit]
     (log-entry this start-commit end-commit)))
  (path [this] (.path this))
  (rev-list [this] (rev-list this)))
