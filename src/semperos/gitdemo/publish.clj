(ns semperos.gitdemo.publish
  (:require [semperos.gitdemo.impl.html :as html]))

(defn html-slideshow
  "Take EDN representation of slideshow and produce HTML"
  [slideshow]
  (html/slideshow slideshow))
