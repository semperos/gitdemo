(ns semperos.gitdemo.protocols)

(defprotocol IGit
  (file [this commit file-name] "Contents of file at commit. Akin to `git show`")
  (log-entry
    [this commit]
    [this start-commit end-commit])
  (path [this] "Path to active Git repository")
  (rev-list [this]))
